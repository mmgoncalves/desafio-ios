//
//  ServiceDelegate.swift
//  list-github-repository
//
//  Created by Mateus Marques on 10/11/17.
//  Copyright © 2017 Mateus Marques. All rights reserved.
//

import Foundation

protocol ServiceDelegate {
    func onFinish() -> Void
}
